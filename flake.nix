{
  description = "BRUTE AE Sync";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nix2container.url = "github:nlewo/nix2container";

  outputs = { self, nixpkgs, flake-utils, nix2container }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system}.pkgsMusl;
      nix2containerPkgs = nix2container.packages.x86_64-linux;
      brutegit-ae-sync = with pkgs;
        stdenvNoCC.mkDerivation (finalAttrs: {
          name = "brutegit-ae-sync";
          dontUnpack = true;
          propagatedBuildInputs = [ gitMinimal coreutils gawk gnused findutils ];
          postInstall = ''
          mkdir -p $out/bin
          cp ${./brutegit-ae-sync} $out/bin/brutegit-ae-sync
        '';
        });
    in {
      packages = { inherit brutegit-ae-sync; };

      ################
      # Docker image #
      ################
      # Create the image with: nix run .#container.copyToDockerDaemon
      packages.container = nix2containerPkgs.nix2container.buildImage
        (let
          gitconfig = pkgs.writeText "gitconfig" ''
            [user]
            email = brutegit-ae-sync@example.com
            name = brutegit-ae-sync
          '';
          user = "root";
          group = "root";
          uid = "0";
          gid = "0";
          rootDirs = pkgs.runCommand "root" {} ''
            mkdir -p $out/etc
            mkdir -p $out/tmp
            mkdir -p $out/root
            cp ${gitconfig} $out/root/.gitconfig

            echo "${user}:x:${uid}:${gid}::/root:" > $out/etc/passwd
            echo "${user}:!x:::::::" > $out/etc/shadow

            echo "${group}:x:${gid}:" > $out/etc/group
            echo "${group}:x::" > $out/etc/gshadow
          '';
        in {
          name = "wentasah/brutegit-ae-sync";
          tag = "latest";
          copyToRoot = [
            # When we want tools in /, we need to symlink them in order to
            # still have libraries in /nix/store. This behavior differs from
            # dockerTools.buildImage but this allows to avoid having files
            # in both / and /nix/store.
            (pkgs.buildEnv {
              name = "root";
              paths =
                [ brutegit-ae-sync ]
                ++ brutegit-ae-sync.propagatedBuildInputs
                ++ (with pkgs; [
                  bash openssh gnugrep
                  gettext       # for envsubst
                ]);
              pathsToLink = [ "/bin" ];
            })
            rootDirs
          ];
          perms = [
            { path = rootDirs; regex = "/tmp"; mode = "0777"; }
            { path = rootDirs; regex = "/etc/.*shadow"; mode = "0600"; }
          ];
          config = {
            Cmd = [ "/bin/bash" ];
            Env = [
              "HOME=/root"
              "USER=user"
              "SSL_CERT_FILE=${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
            ];
          };
        });
    });
}
