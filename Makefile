INPUT = 1f6e3f62c730ae432b42eba1d25c827660e8134b \
	$(shell git rev-parse master) \
	refs/heads/master

.PHONY: test
test:
	git branch -D ae01 ae02 root-test root-test-subdir || :
	echo $(INPUT) | bash ./brutegit-ae-sync

image:
	docker build -t wentasah/brutegit-ae-sync .

push:
	docker push wentasah/brutegit-ae-sync
