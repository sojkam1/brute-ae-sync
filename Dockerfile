FROM debian:bookworm-slim
RUN apt-get update && apt-get install -y git gettext-base && apt-get clean
COPY brutegit-ae-sync /usr/local/bin
