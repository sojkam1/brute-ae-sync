# brutegit-ae-sync

Script to perform automatic mirroring of a **subset** of a git
repository to another git repository. Initially, this was designed for
updating multiple [BRUTE][] [automatic evaluation][] (AE) git
[repositories][] from the main course repository. Now, it can be used
with arbitrary git repositories as targets.

With [BRUTE][] it works as follows: After pushing to the main course
git repository, this script creates one or more subsets of the main
repository containing only files for AE and pushes these subsets to
BRUTE's AE git repositories. Which files form the subsets and where to
push them is configured in the `.brutegit-ae-sync` file in the root
directory of the repo. See `.brutegit-ae-sync` file in this repo for
an example.

[BRUTE]: https://cw.felk.cvut.cz/brute/
[automatic evaluation]: https://cw.fel.cvut.cz/wiki/help/teachers/brute/ae_docker
[repositories]: https://brutegit.felk.cvut.cz/

## Usage

You can use `brutegit-ae-sync` in at least four different ways:

- As git **post-receive hook** on the server. Just rename it and place it
  to `.git/hooks`. This usually requires having shell access to the
  git server.
- Via **Gitlab webhook** with the help of
  [gitlab-webhook-receiver.py](./gitlab-webhook-receiver.py) (perhaps
  run from [gitlab-ae-sync@.service systemd
  service](./gitlab-ae-sync@.service) on your server).
- Inside of **Gitlab CI** (other forges will probably work too).
- Locally to test how the script works.

The two latter options are documented below.

### Local testing

You can test this script in your local repository by giving it a
commit as an argument:

    brutegit-ae-sync HEAD~1
    brutegit-ae-sync HEAD

When running these two commands in this repository, they create the AE
branches (according to the `.brutegit-ae-sync` file) with one or two
commits (depending on whether the AE data were changed between HEAD~1
and HEAD). You can see the commit(s) with:

    git log ae01 ae02

If you're happy with the result, you can *force-push* the branches
to their corresponding repositories by running:

    brutegit-ae-sync --push

If pushing fails because you cannot force-push to the remote repo,
run:

    brutegit-ae-sync --fetch

and repeat all steps above. This ensures that the new commits are
created on top of what is already in the remote repo(s).

### Gitlab CI

To run this script inside Gitlab CI, configure it as follows (replace
`ESW` with whatever you want):

1. Generate an SSH key pair for the CI, e.g.:

       ssh-keygen -t ed25519 -C 'gitlab.fel.cvut.cz/esw' -f id

2. Add the public key in `id.pub` to your BRUTE gitlab account.
3. Add the private key as a variable to the Gitlab repository where
   you run the CI. Go to *Settings* → *CI/CD Settings* → *Variables*
   and click *Add variable*. Fill in the following values:
   - Type: `File`
   - Protect variable: ✓ (recommended, not necessary)
   - Key: `SSH_PRIVATE_KEY`
   - Value: *the content of the `id` file*

   Ensure that the value ends with newline. Otherwise, you'll see
   error like this:
   > Error loading key "/builds/psr/psr.tmp/SSH_PRIVATE_KEY": error in libcrypto

4. [Configure SSH host keys for the
   CI](https://docs.gitlab.com/ee/ci/ssh_keys/#verifying-the-ssh-host-keys). Run:

       ssh-keyscan brutegit.felk.cvut.cz

    and add the output of the command as another CI/CD file variable
    named `SSH_KNOWN_HOSTS`.
5. Add file `.gitlab-ci.yml` with the following content to your repo:

      ```yaml
      brutegit-ae-sync:
        image: wentasah/brutegit-ae-sync
        rules:
          - if: $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
        before_script:
          # https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
          - eval $(ssh-agent -s)
          - chmod 400 "$SSH_PRIVATE_KEY"
          - ssh-add "$SSH_PRIVATE_KEY"
          - mkdir -p ~/.ssh
          - chmod 700 ~/.ssh
          - git config --global user.email "esw-bot@gitlab"
          - git config --global user.name "ESW bot"
          # https://docs.gitlab.com/ee/ci/ssh_keys/#verifying-the-ssh-host-keys
          - cp "$SSH_KNOWN_HOSTS" ~/.ssh/known_hosts
          - chmod 644 ~/.ssh/known_hosts
        script:
          - brutegit-ae-sync --fetch
          - echo "$CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA refs/heads/$CI_COMMIT_REF_NAME" | brutegit-ae-sync
      ```

   This uses a [Docker image][] that can be recreated from this
   repository by running:

        nix run .#container.copyToDockerDaemon

[Docker image]: https://hub.docker.com/r/wentasah/brutegit-ae-sync

#### Using Gitlab CI to synchronize to other Gitlab repositories via HTTPS

If you want to use this script to synchronize not only to BRUTE but
also to other repositories on the same (or other) Gitlab instance, you
may want to prefer using git over HTTPS instead of SSH. The reason is
that if you cannot create separate gitlab user for this, you would
need to add the SSH key to your account and everybody with access to
your CI repo (typically all teachers) could access all your
repositories including private ones.

To use this script with repositories over HTTPS configure the repo URL
in `.brutegit-ae-sync` as

    url = https://gitlab-ci-token:${ACCESS_TOKEN}@gitlab.fel.cvut.cz/owner/repo.git

and add a variable named `ACCESS_TOKEN` in *Settings* → *CI/CD
Settings* → *Variables*. Check protect and mask checkboxes in variable
configuration. The value will be the access token created for the
target repository in *Settings* → *Access tokens* and you need enable
`read_repository` and `write_repository` scopes.

## Switching to new BRUTE repository

BRUTE requires to create new AE repositories every year. Therefore,
every year, you have to update the configuration in
`.brutegit-ae-sync` to point to the new repos.

BRUTE repos do not allow force-pushing to the master branch.
Therefore, after changing the configuration, you have to ensure that
`brutegit-ae-sync` will not try to push unrelated commit history to
the BRUTE repos. The easiest way is to do that is by running the
following command in the server repository with the updated
configuration:

    brutegit-ae-sync --fetch
